"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const util = __importStar(require("util"));
const fsPromise = util.promisify(fs.readFile);
function Parser() {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield fsPromise(path.join(__dirname, 'grammar.peg'));
        console.log(result);
    });
}
exports.Parser = Parser;
Parser().then((data) => console.log("Success", data)).catch((err) => console.log(err, "Error"));
// fs.readFile(), 'utf-8', function (err, data) {
//     if (err) {
//         callback(err);
//     }
//     else {
//         callback(null, pegjs.generate(data));
//     }
// });
//# sourceMappingURL=index.js.map