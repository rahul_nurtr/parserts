"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
index_1.Parser((err, parser) => {
    if (err)
        return err;
    const [result] = parser.parse(`{***} 13. Nxd4 {capture} Qxd4 14. Nd5 $1 {threat} Qc5 15. Bxf6 $1 {capture}
        gxf6 (15... Bxf6 16. Qe4 $1 {threat} (16. Qh5 $1 h6 17. Nxf6+ gxf6 18. Qxh6 f5
        19. Rae1 $1 $18 Qc6 20. Be4 $1 fxe4 21. Qg5+ Kh7 22. Re3 $1 $18) 16... g6 17.
        Nxf6+ {check} Kg7 18. Qxb7 $18 {capture}) 16. Nxe7+ {check} Qxe7 17. Qg4+ {
        check} Kh8 18. Qh4 $18 {threat} 1-0`);
    console.log(result.moves[0]);
});
//# sourceMappingURL=test.js.map