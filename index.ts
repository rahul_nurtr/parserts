import * as pegjs from 'pegjs';
import * as fs from 'fs';
import * as path from 'path';

export function Parser() {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, 'grammar.peg'), 'utf-8', (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                return resolve(pegjs.generate(data));
            }
        });
    });
}