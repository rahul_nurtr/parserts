"use strict";
exports.__esModule = true;
var pegjs = require("pegjs");
var fs = require("fs");
var path = require("path");
function Parser() {
    return new Promise(function (resolve, reject) {
        fs.readFile(path.join(__dirname, 'grammar.peg'), 'utf-8', function (err, data) {
            if (err) {
                return reject(err);
            }
            else {
                return resolve(pegjs.generate(data));
            }
        });
    });
}
exports.Parser = Parser;
